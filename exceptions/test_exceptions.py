from fastapi import FastAPI
from fastapi.testclient import TestClient

import exceptions as ex
from service import api


def test_no_exceptions():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    @svc.get('/')
    def test():
        return {}

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 200
    assert resp.json() == {}


def test_exception_mid_customer_exists():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    @svc.get('/')
    def test():
        raise ex.CustomerAlreadyExists

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 409
    assert resp.json() == {'message': api.Messages.customer_exists}


def test_exception_mid_customer_not_found():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    @svc.get('/')
    def test():
        raise ex.CustomerNotFound

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 404
    assert resp.json() == {'message': api.Messages.customer_not_found}


def test_exception_mid_product_in_favorites():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    @svc.get('/')
    def test():
        raise ex.ProductAlreadyFavorite

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 409
    assert resp.json() == {'message':
                           api.Messages.product_already_in_favorites}


def test_exception_mid_product_not_favorite():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    @svc.get('/')
    def test():
        raise ex.ProductNotFavorite

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 409
    assert resp.json() == {'message':
                           api.Messages.product_not_in_favorites}


def test_exception_mid_unknown_error():
    svc = FastAPI()
    svc.add_middleware(ex.ExceptionMiddleware)

    class RandomError(Exception):
        pass

    @svc.get('/')
    def test():
        raise RandomError

    client = TestClient(svc)
    resp = client.get('/')

    assert resp.status_code == 500
    assert resp.json() == {'message': api.Messages.internal_error}
