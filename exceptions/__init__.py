import sys
import logging

from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware

from product_api import ProductNotFound
from service import api


class CustomerAlreadyExists(Exception):
    pass


class CustomerNotFound(Exception):
    pass


class ProductAlreadyFavorite(Exception):
    pass


class ProductNotFavorite(Exception):
    pass


def get_logger():
    logger = logging.Logger('favorite-list', level=logging.INFO)
    handler = logging.StreamHandler(stream=sys.stdout)
    format = '{"level": "%(levelname)s", "date": "%(asctime)s", "location": "%(funcName)s/%(filename)s:%(lineno)s", "msg": "%(message)s"}'
    handler.setFormatter(logging.Formatter(format))
    logger.addHandler(handler)

    return logger


class ExceptionMiddleware(BaseHTTPMiddleware):
    def __init__(self, app):
        super().__init__(app)
        self.app = app
        self.log = get_logger()

    async def dispatch(self, request: Request, call_next):
        try:
            response = await call_next(request)
            return response

        except CustomerAlreadyExists:
            return JSONResponse({
                'message': api.Messages.customer_exists
            }, status_code=409)

        except CustomerNotFound:
            return JSONResponse({
                'message': api.Messages.customer_not_found
            }, status_code=404)

        except ProductAlreadyFavorite:
            return JSONResponse({
                'message': api.Messages.product_already_in_favorites
            }, status_code=409)

        except ProductNotFavorite:
            return JSONResponse({
                'message': api.Messages.product_not_in_favorites
            }, status_code=409)

        except Exception as err:
            self.log.error(f'unknown error: {err}')

            return JSONResponse({
                'message': api.Messages.internal_error
            }, status_code=500)
