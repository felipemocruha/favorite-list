FROM python:3.7 AS builder

COPY requirements.txt /
RUN apt update -y \
	&& apt install libev-dev -y \
    && pip install -r requirements.txt


FROM debian:10-slim

RUN apt update -y && apt install python3 -y

COPY main.py  /
COPY exceptions /exceptions
COPY repository /repository
COPY product_api /product_api
COPY service /service

COPY --from=builder /usr/local/lib/python3.7/site-packages /usr/local/lib/python3.7/site-packages
COPY --from=builder /usr/local/bin/gunicorn /usr/local/bin
COPY --from=builder /usr/lib/x86_64-linux-gnu/libev.so.4 /usr/lib
COPY --from=builder /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu
ENV PYTHONPATH=/usr/local/lib/python3.7/site-packages

CMD ["python3", "/usr/local/bin/gunicorn", "-k", "uvicorn.workers.UvicornWorker", "main:service", "-b", "0.0.0.0:9595", "-w", "9"]
