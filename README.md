# Favorite Products API

## Overview

This application is responsible for managing customers and their favorite products.

![diagram](https://gitlab.com/felipemocruha/favorite-list/uploads/e1282f27100629f7292472fa8502655b/Untitled_Diagram.png)

It has 3 major components:
 - **Service**: exposes the service functionality over a REST API
 - **PostgreSQL**: the database where service's informations are stored
 - **Product API**: a service responsible for managing product info

And the the internal modules:
 - **exceptions**: a centralized module to handle errors
 - **product_api**: a simple client to get data from the Product API
 - **repository**: manages data from database
 - **service**: manages authentication and REST API

## Running

Clone this repo:

```
git clone https://gitlab.com/felipemocruha/favorite-list.git
cd favorite-list
```

### With Docker

There is already a built image from the CI/CD pipeline. To simply run the service and database:

```
make containers
```

This will pull the images from gitlab and run the docker containers.

**Make sure to have ports 9595 and 5432 unused or this will fail.**

To stop running them:

```
make clean_containers
```

### Authentication and Authorization

To make requests to protected resources of the API, you have to specify a header 'X-API-Key' on every request.

Here is an example using ```requests``` and ```python```:

```
import requests

r = requests.get('http://localhost:9095/customers', headers={'X-API-Key': 'some key'})
```

There are two user roles:
 - **admin**: can access all resources (default api_key: '5207b4a1-ab40-48dc-9405-393d37d26500')
 - **reader**: can only execute GET endpoints (default api_key: '6b41f0fb-a923-455e-8379-9915773fccf4')

### API Documentation

You can view a live documentation of the API accessing http://localhost:9595/docs when the server is running.
It is possible to visualize all the request and response schemas supported.

![example](https://gitlab.com/felipemocruha/favorite-list/uploads/e7afa93e461bdc39ec2eabc9c190ad7b/openapi.png)

### Running locally

```
pipenv install -d
pipenv shell

make postgres
make run
```

## Testing

### Unit tests

run using pytest:

```
pipenv install -d
pipenv shell
make coverage
```

### End-to-end

There is an end-to-end test of a customer flow under ```e2e``` directory.
To run the test, make sure no containers from previous steps are running. Then:

```
pipenv install -d
pipenv shell
make e2e
```

### Observability

The service exposes prometheus metrics on http://localhost:9595/metrics and the logs are sent to STDOUT in json format.

## Configuration

The application comes with defaults that work with all the scripts provided.

To run an alternative configuration, you can set the environment variable ```CONFIG_PATH``` pointing to a json file with the following structure:

```
{
    "db": {
        "host": "0.0.0.0",
        "port": "5432",
        "user": "favorites",
        "password": "favorites",
        "database": "favorites"
    },
    "api_keys": {
        "admin": "5207b4a1-ab40-48dc-9405-393d37d26500",
        "reader": "6b41f0fb-a923-455e-8379-9915773fccf4"
    }
}
```
