from fastapi.testclient import TestClient
from fastapi import FastAPI

from service import auth
from service import api


def test_auth_middleware_allowed_path():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    @svc.get('/healthz')
    def healthcheck():
        return {}

    client = TestClient(svc)
    resp = client.get('/healthz')

    assert resp.status_code == 200


def test_auth_middleware_no_key_provided():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    @svc.get('/')
    def test():
        pass

    expected = {'message': api.Messages.auth_header_required}
    client = TestClient(svc)
    resp = client.get('/')


    assert resp.status_code == 401
    assert resp.json() == expected


def test_auth_middleware_invalid_key():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    @svc.get('/')
    def test():
        pass

    expected = {'message': api.Messages.invalid_credentials}
    client = TestClient(svc)
    resp = client.get('/', headers={'X-API-Key': 'h4ck3r'})

    assert resp.status_code == 401
    assert resp.json() == expected


def test_auth_middleware_insufficient_permissions():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    @svc.delete('/criticalresource')
    def test():
        pass

    expected = {'message': api.Messages.insufficient_permissions}
    client = TestClient(svc)
    resp = client.delete('/criticalresource',
                      headers={'X-API-Key': config['reader']})

    assert resp.status_code == 403
    assert resp.json() == expected


def test_auth_middleware_ok_reader():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    expected = {'ok': True}
    @svc.get('/')
    def test():
        return expected

    client = TestClient(svc)
    resp = client.get('/', headers={'X-API-Key': config['reader']})

    assert resp.status_code == 200
    assert resp.json() == expected


def test_auth_middleware_ok_admin():
    svc = FastAPI()
    config = {'admin': 'admin', 'reader': 'reader'}
    auth.create_auth_middleware(svc, config)

    expected = {'ok': True}
    @svc.delete('/')
    def test():
        return expected

    client = TestClient(svc)
    resp = client.delete('/', headers={'X-API-Key': config['admin']})

    assert resp.status_code == 200
    assert resp.json() == expected
