from enum import Enum
from typing import List, Optional
from pydantic import BaseModel


class Messages(str, Enum):
    internal_error = "oops, something wrong happened. Try again later."
    customer_created = "customer created successfully."
    customer_deleted = "customer deleted successfully."
    customer_updated = "customer updated successfully."
    customer_exists = "customer already exists!"
    customer_not_found = "customer not found!"
    unknown_product = "product does not exist!"
    product_already_in_favorites = "product already in favorite list!"
    product_not_in_favorites = "product not in favorite list!"
    favorite_added = "product has been added to favorite list!"
    auth_header_required = "'X-API-Key' header is required to authenticate!"
    invalid_credentials = "invalid credentials provided!"
    insufficient_permissions = "client does not have enough permissions to perform this action"


class ResponseMessage(BaseModel):
    message: Messages


class NewCustomer(BaseModel):
    email: str
    name: str


class CreateCustomerResponse(BaseModel):
    message: str
    id: Optional[str]


class UpdateCustomer(BaseModel):
    email: Optional[str]
    name: Optional[str]


class UpdateCustomerResponse(BaseModel):
    message: str


class GetCustomerResponse(BaseModel):
    message: Optional[str]
    email: Optional[str]
    name: Optional[str]


class ListCustomerResponse(BaseModel):
    message: Optional[str]
    customers: Optional[List[NewCustomer]]


class AddFavoriteRequest(BaseModel):
    product_id: str


class AddFavoriteResponse(BaseModel):
    message: str


class FavoriteList(BaseModel):
    product_ids: List[str] = []


class ProductDetails(BaseModel):
    id: str
    title: str
    price: float
    image: str
    reviewScore: Optional[float]
