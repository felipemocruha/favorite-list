from fastapi import Response
from starlette_prometheus import metrics, PrometheusMiddleware

import exceptions as ex
import repository
from service import auth
from service import api
from product_api import ProductNotFound


async def build_service(service, db, product_api, config):
    repo = repository.Repository(db)
    auth.create_auth_middleware(service, config['api_keys'])
    create_routes(service, repo, product_api)

    return service


def create_routes(service, repo, product_api):
    service.add_middleware(ex.ExceptionMiddleware)
    service.add_middleware(PrometheusMiddleware)
    service.add_route('/metrics', metrics)

    @service.post(
        "/customers",
        response_model=api.CreateCustomerResponse,
        response_model_exclude_unset=True,
        tags=['customer']
    )
    async def create_customer(resp: Response, customer: api.NewCustomer):
        id = await repo.create_customer(customer.email, customer.name)
        return {
            'message': api.Messages.customer_created,
            'id': id
        }


    @service.get(
        "/customers",
        response_model=api.ListCustomerResponse,
        response_model_exclude_unset=True,
        tags=['customer']
    )
    async def list_customers(resp: Response):
        customers = await repo.list_customers()
        return {'customers': customers}


    @service.get(
        "/customers/{id}",
        response_model=api.GetCustomerResponse,
        response_model_exclude_unset=True,
        tags=['customer']
    )
    async def get_customer(id: str, resp: Response):
        return await repo.get_customer(id)


    @service.patch(
        "/customers/{id}",
        response_model=api.UpdateCustomerResponse,
        tags=['customer']
    )
    async def update_customer(
            id: str, customer: api.UpdateCustomer, resp: Response):
        await repo.update_customer(id, customer.email, customer.name)
        return {
            'message': api.Messages.customer_updated
        }


    @service.delete(
        "/customers/{id}",
        response_model=api.ResponseMessage,
        response_model_exclude_unset=True,
        tags=['customer']
    )
    async def delete_customer(id: str, resp: Response):
        await repo.delete_customer(id)

        return {
            'message': api.Messages.customer_deleted
        }


    @service.post(
        "/customers/{id}/favorites",
        response_model=api.ResponseMessage,
        response_model_exclude_unset=True,
        tags=['favorites']
    )
    async def add_favorite(
            id: str, req: api.AddFavoriteRequest, resp: Response):
        info = product_api.get_product_info(req.product_id)
        await repo.add_favorite(id, req.product_id)

        return {
            'message': api.Messages.favorite_added,
        }


    @service.get(
        "/customers/{id}/favorites",
        response_model=api.FavoriteList,
        response_model_exclude_unset=True,
        tags=['favorites']
    )
    async def list_favorites(id: str, resp: Response):
        return await repo.list_favorites(id)


    @service.get(
        "/customers/{id}/favorites/{product_id}",
        response_model=api.ProductDetails,
        response_model_exclude_unset=True,
        tags=['favorites']
    )
    async def get_product_details(id: str, product_id: str, resp: Response):
        await repo.favorite_exists_or_raise(id, product_id)
        return product_api.get_product_info(product_id)


    @service.get("/healthz")
    async def healthcheck():
        return {}
