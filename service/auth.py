from pydantic import BaseModel
from fastapi import Request
from fastapi.responses import JSONResponse
from starlette.routing import Match

from service import api


AUTH_KEY = 'X-API-Key'

class AuthConfig(BaseModel):
    admin: str
    reader: str


def _auth_header_required():
    return JSONResponse({
        'message': api.Messages.auth_header_required
    }, status_code=401)


def _invalid_credentials():
    return JSONResponse({
        'message': api.Messages.invalid_credentials
    }, status_code=401)


def _get_role(api_key, config):
    if api_key == config['admin']:
        return 'admin'

    elif api_key == config['reader']:
        return 'reader'

    else:
        None


def _insufficient_permissions():
    return JSONResponse({
        'message': api.Messages.insufficient_permissions
    }, status_code=403)


def _get_path_template(request: Request) -> str:
    for route in request.app.routes:
        match, child_scope = route.matches(request.scope)

        if match == Match.FULL:
            return route.path

    return request.url.path


def create_auth_middleware(service, config: AuthConfig):
    @service.middleware('http')
    async def dispatch(request: Request, call_next):
        allowed_paths = ['/docs', '/healthz', '/metrics', '/openapi.json']

        # allow open resources
        if _get_path_template(request) in allowed_paths:
            response = await call_next(request)
            return response

        method = request.method
        api_key = request.headers.get(AUTH_KEY)

        if not api_key:
            return _auth_header_required()

        role = _get_role(api_key, config)
        if not role:
            return _invalid_credentials()

        if role != 'admin' and method != 'GET':
            return _insufficient_permissions()

        response = await call_next(request)
        return response

    return dispatch
