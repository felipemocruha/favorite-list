import pytest
from mock import MagicMock
from fastapi import FastAPI
from fastapi.testclient import TestClient

import product_api
import exceptions as ex
from service import api
from service import service


def test_create_customer_ok():
    async def side_effect(*_):
        return 'new_uuid'

    svc = FastAPI()
    repo = MagicMock(create_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)
    customer = {'email': 'test@mail.com', 'name': 'testevaldo'}

    resp = client.post('/customers', json=customer)

    assert resp.status_code == 200
    assert resp.json() == {'message': api.Messages.customer_created,
                         'id': 'new_uuid'}


def test_create_customer_exists():
    async def side_effect(*_):
        raise ex.CustomerAlreadyExists

    svc = FastAPI()
    repo = MagicMock(create_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)
    customer = {'email': 'test@mail.com', 'name': 'testevaldo'}

    resp = client.post('/customers', json=customer)

    assert resp.status_code == 409
    assert resp.json() == {'message': api.Messages.customer_exists}


def test_list_customers_ok():
    async def side_effect(*_):
        return []

    svc = FastAPI()
    repo = MagicMock(list_customers=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.get('/customers')

    assert resp.status_code == 200
    assert resp.json() == {'customers': []}


def test_get_customer_ok():
    expected = {'email': 'test@mail.com', 'name': 'testevaldo'}
    async def side_effect(*_):
        return expected

    svc = FastAPI()
    repo = MagicMock(get_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.get('/customers/id')

    assert resp.status_code == 200
    assert resp.json() == expected


def test_get_customer_not_found():
    async def side_effect(*_):
        raise ex.CustomerNotFound

    svc = FastAPI()
    repo = MagicMock(get_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.get('/customers/id')

    assert resp.status_code == 404
    assert resp.json() == {'message': api.Messages.customer_not_found}


def test_delete_customer_ok():
    async def side_effect(*_):
        pass

    svc = FastAPI()
    repo = MagicMock(delete_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.delete('/customers/id')

    assert resp.status_code == 200
    assert resp.json() == {'message': api.Messages.customer_deleted}


def test_delete_customer_not_found():
    async def side_effect(*_):
        raise ex.CustomerNotFound

    svc = FastAPI()
    repo = MagicMock(delete_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.delete('/customers/id')

    assert resp.status_code == 404
    assert resp.json() == {'message': api.Messages.customer_not_found}


def test_add_favorite_ok():
    product = {
        'id': 'test',
        'title': 'Teste',
        'image': 'http://images.test.com/some-uuid',
        'price': 322.59
    }

    def json():
        return product

    def get(*args, **kwargs):
        return MagicMock(status_code=200, json=json)

    client = MagicMock(get=get)
    prod_api = product_api.ProductAPI(client, 'http://somewhere.com')

    async def side_effect(*_):
        pass

    svc = FastAPI()
    repo = MagicMock(add_favorite=side_effect)

    service.create_routes(svc, repo, prod_api)
    client = TestClient(svc)
    fav = {'product_id': 'om23jh242'}

    resp = client.post('/customers/id/favorites', json=fav)

    assert resp.status_code == 200
    assert resp.json() == {'message': api.Messages.favorite_added}


def test_add_favorite_exists():
    product = {
        'id': 'test',
        'title': 'Teste',
        'image': 'http://images.test.com/some-uuid',
        'price': 322.59
    }

    def json():
        return product

    def get(*args, **kwargs):
        return MagicMock(status_code=200, json=json)

    client = MagicMock(get=get)
    prod_api = product_api.ProductAPI(client, 'http://somewhere.com')

    async def side_effect(*_):
        raise ex.ProductAlreadyFavorite

    svc = FastAPI()
    repo = MagicMock(add_favorite=side_effect)

    service.create_routes(svc, repo, prod_api)
    client = TestClient(svc)
    fav = {'product_id': 'om23jh242'}

    resp = client.post('/customers/id/favorites', json=fav)

    assert resp.status_code == 409
    assert resp.json() == {'message': api.Messages.product_already_in_favorites}


def test_list_favorites_ok():
    expected = {'product_ids': ['some_uuid']}

    async def side_effect(*_):
        return expected

    svc = FastAPI()
    repo = MagicMock(list_favorites=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.get('/customers/id/favorites')

    assert resp.status_code == 200
    assert resp.json() == expected


def test_get_product_details_ok():
    expected =  {
        'id': 'test',
        'title': 'Teste',
        'image': 'http://images.test.com/some-uuid',
        'price': 322.59
    }

    def info(*_):
        return expected

    prod_api = MagicMock(get_product_info=info)

    async def side_effect(*_):
        return {'exists': True}

    svc = FastAPI()
    repo = MagicMock(favorite_exists_or_raise=side_effect)

    service.create_routes(svc, repo, prod_api)
    client = TestClient(svc)

    resp = client.get('/customers/id/favorites/23k23')

    assert resp.status_code == 200
    assert resp.json() == expected


def test_get_product_details_not_found():
    async def side_effect(*_):
        raise ex.ProductNotFavorite

    svc = FastAPI()
    repo = MagicMock(favorite_exists_or_raise=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    resp = client.get('/customers/{id}/favorites/{prod_id}')

    assert resp.status_code == 409
    assert resp.json() == {'message': api.Messages.product_not_in_favorites}


def test_update_customer_ok():
    async def side_effect(*_):
        pass

    svc = FastAPI()
    repo = MagicMock(update_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    customer = {'email': 'test@mail.com', 'name': 'outrotestevaldo'}
    resp = client.patch('/customers/{id}', json=customer)

    assert resp.status_code == 200
    assert resp.json() == {'message': api.Messages.customer_updated}


def test_update_customer_not_found():
    async def side_effect(*_):
        raise ex.CustomerNotFound

    svc = FastAPI()
    repo = MagicMock(update_customer=side_effect)

    service.create_routes(svc, repo, None)
    client = TestClient(svc)

    customer = {'email': 'test@mail.com', 'name': 'outrotestevaldo'}
    resp = client.patch('/customers/{id}', json=customer)

    assert resp.status_code == 404
    assert resp.json() == {'message': api.Messages.customer_not_found}
