VERSION="v1.0.0"

run:
	@uvicorn main:service --host 0.0.0.0 --port 9595 --reload

test:
	@pytest -W ignore -v --ignore e2e

coverage:
	@pytest -W ignore -v --cov=repository --cov=service --cov=product_api --cov=exceptions --ignore e2e

prod:
	@gunicorn -k uvicorn.workers.UvicornWorker main:service -b 0.0.0.0:9595 -w 9

postgres:
	docker run -d --name favorite-list --network=host -e POSTGRES_USER=favorites -e POSTGRES_PASSWORD=favorites -e POSTGRES_DB=favorites -e PGDATA=/var/lib/postgresql/data/pgdata postgres:12-alpine

loadtest-server:
	@taskset -c 1,2,3,4 gunicorn -k uvicorn.workers.UvicornWorker main:service -b 0.0.0.0:9595 -w 9

loadtest:
	@taskset -c 5,6,7,8 wrk -c 1000 -t 9 -d 1m http://localhost:9595/customers/a

build:
	pipenv lock -r > requirements.txt
	@docker build -t registry.gitlab.com/felipemocruha/favorite-list:v1.0.0 -f Dockerfile .

build_ci:
	@docker build -t ${IMAGE_TAG} -f Dockerfile .

containers:
	@docker run -d --name favorite-list-db --network=host -e POSTGRES_USER=favorites -e POSTGRES_PASSWORD=favorites -e POSTGRES_DB=favorites -e PGDATA=/var/lib/postgresql/data/pgdata postgres:12-alpine
	@docker run -d --name favorite-list-service --network=host registry.gitlab.com/felipemocruha/favorite-list:v1.0.0

clean_containers:
	@docker rm --force favorite-list-db favorite-list-service

e2e:
#	@$(MAKE) build
	@echo **Make sure you have ports 5432 and 9595 unused**
	@echo Running Database...
	@docker run -d --name e2e-db --network=host -e POSTGRES_USER=favorites -e POSTGRES_PASSWORD=favorites -e POSTGRES_DB=favorites -e PGDATA=/var/lib/postgresql/data/pgdata postgres:12-alpine
	@echo
	@echo Running Service...
	@sleep 5
	@docker run -d --name e2e-service --network=host registry.gitlab.com/felipemocruha/favorite-list:v1.0.0
	@echo
	@echo Starting e2e script...
	@sleep 10
	-@python e2e/e2e.py
	-@docker rm --force e2e-db e2e-service

tag:
	@git tag -a $(VERSION) -m "Release new version"
	@git push origin --tag

.PHONY: build e2e
