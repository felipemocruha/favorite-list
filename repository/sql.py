
SCHEMA = '''
CREATE TABLE IF NOT EXISTS customer(
  id text PRIMARY KEY,
  email text UNIQUE,
  name text
);

CREATE TABLE IF NOT EXISTS favorite_list(
  customer_id text REFERENCES customer(id) ON DELETE CASCADE,
  product_id text,
  PRIMARY KEY(customer_id, product_id)
);
'''

NEW_CUSTOMER = '''
INSERT INTO customer(id, email, name) VALUES($1, $2, $3)
'''

LIST_CUSTOMERS = '''
SELECT email, name FROM customer
'''

GET_CUSTOMER_BY_ID = '''
SELECT email, name FROM customer WHERE id = $1
'''

UPDATE_CUSTOMER_EMAIL = '''
UPDATE customer SET email = $2 WHERE id = $1 RETURNING id
'''

UPDATE_CUSTOMER_NAME = '''
UPDATE customer SET name = $2 WHERE id = $1 RETURNING id
'''

DELETE_CUSTOMER_BY_ID = '''
DELETE FROM customer WHERE id = $1
'''

LIST_FAVORITES = '''
SELECT customer_id, product_id FROM favorite_list WHERE customer_id = $1
'''

ADD_FAVORITE = '''
INSERT INTO favorite_list(customer_id, product_id) VALUES($1, $2)
'''

DELETE_CUSTOMER_FAVORITES = '''
DELETE FROM favorite_list WHERE customer_id = $1
'''

FAVORITE_EXISTS = '''
SELECT exists
  (SELECT 1 FROM favorite_list WHERE
    customer_id = $1 AND product_id = $2 LIMIT 1
  );
'''
