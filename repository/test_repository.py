from contextlib import asynccontextmanager

import pytest
from asyncpg.exceptions import UniqueViolationError, ForeignKeyViolationError

import repository
import exceptions as ex


class FakeConnection:
    def __init__(self, execute_return_fn, fetch_return_fn):
        self.execute_return_fn = execute_return_fn
        self.fetch_return_fn = fetch_return_fn

    async def execute(self, *args, **kwargs):
        return self.execute_return_fn()

    async def fetch(self, *args, **kwargs):
        return self.fetch_return_fn()


class FakeDB:
    def __init__(self, conn):
        self.conn = conn

    @asynccontextmanager
    async def acquire(self):
        yield self.conn


@pytest.mark.asyncio
async def test_create_customer_ok():
    def exec():
        pass

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)
    resp = await repo.create_customer('test@test.com', 'test')

    assert resp is not None


@pytest.mark.asyncio
async def test_create_customer_already_exists():
    def exec():
        raise UniqueViolationError

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerAlreadyExists):
        await repo.create_customer('test@test.com', 'test')


@pytest.mark.asyncio
async def test_list_customers_ok():
    expected = {'email': 't@test.com', 'name': 'testevaldo'}

    def query():
        return [expected]

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)
    resp = await repo.get_customer('123test')

    assert resp == expected


@pytest.mark.asyncio
async def test_get_customer_ok():
    expected = {'email': 't@test.com', 'name': 'testevaldo'}

    def query():
        return [expected]

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)
    resp = await repo.get_customer('123test')

    assert resp == expected


@pytest.mark.asyncio
async def test_get_customer_not_found():
    def query():
        return []

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerNotFound):
        resp = await repo.get_customer('123test')


@pytest.mark.asyncio
async def test_delete_customer_ok():
    def exec():
        return ''

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    try:
        resp = await repo.delete_customer('123test')
    except:
        # this call should never fail
        assert True is False


@pytest.mark.asyncio
async def test_delete_customer_not_found():
    def exec():
        return repository.NO_RECORDS_DELETED

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerNotFound):
        resp = await repo.delete_customer('123test')


@pytest.mark.asyncio
async def test_add_favorite_ok():
    def exec():
        pass

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    try:
        resp = await repo.add_favorite('c_id', 'p_id')
    except:
        # this call should never fail
        assert True is False


@pytest.mark.asyncio
async def test_add_favorite_already_in_list():
    def exec():
        raise UniqueViolationError

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    with pytest.raises(ex.ProductAlreadyFavorite):
        resp = await repo.add_favorite('c_id', 'p_id')


@pytest.mark.asyncio
async def test_add_favorite_invalid_customer():
    def exec():
        raise ForeignKeyViolationError

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerNotFound):
        resp = await repo.add_favorite('c_id', 'p_id')


@pytest.mark.asyncio
async def test_list_favorites_customer_not_found():
    def query():
        return []

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerNotFound):
        resp = await repo.list_favorites('123test')


@pytest.mark.asyncio
async def test_list_favorites_ok():
    expected = {'product_ids': ['prod_id']}
    def query():
        return [{'product_id': 'prod_id'}]

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)
    resp = await repo.list_favorites('123test')

    assert resp == expected


@pytest.mark.asyncio
async def test_favorite_exists_product_not_favorite():
    def query():
        return [{'exists': False}]

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)

    with pytest.raises(ex.ProductNotFavorite):
        resp = await repo.favorite_exists_or_raise('123test', 'test123')


@pytest.mark.asyncio
async def test_favorite_exists_product_not_favorite():
    def query():
        return [{'exists': False}]

    db = FakeDB(FakeConnection(None, query))
    repo = repository.Repository(db)

    with pytest.raises(ex.ProductNotFavorite):
        resp = await repo.favorite_exists_or_raise('123test', 'test123')


@pytest.mark.asyncio
async def test_update_customer_ok():
    def exec():
        pass

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    try:
        resp = await repo.update_customer('121h32j2h13', 'test@test.com', 'test')
    except:
        # this call should never fail
        assert True is False


@pytest.mark.asyncio
async def test_update_customer_doesnt_exist():
    def exec():
        return repository.NO_RECORDS_UPDATED

    db = FakeDB(FakeConnection(exec, None))
    repo = repository.Repository(db)

    with pytest.raises(ex.CustomerNotFound):
        resp = await repo.update_customer(
            '121h32j2h13', 'test@test.com', 'test')
