import os
from uuid import uuid4

from pydantic import BaseModel
import asyncpg
from asyncpg.exceptions import UniqueViolationError, ForeignKeyViolationError

from repository import sql
import exceptions as ex


NO_RECORDS_UPDATED = 'UPDATE 0'
NO_RECORDS_DELETED = 'DELETE 0'


class DbConfig(BaseModel):
    host: str = 'localhost'
    port: str = '5432'
    user: str
    password: str
    database: str = 'favorites'


async def connect_to_db(db_config: DbConfig):
    try:
        conn = await asyncpg.create_pool(**db_config)
        await conn.execute(sql.SCHEMA)
        return conn

    except Exception as err:
        log = ex.get_logger()
        log.fatal(f'failed to connect to db: {err}')
        os._exit(1)


class Repository:
    def __init__(self, db):
        self.db = db

    async def _execute(self, *params):
        async with self.db.acquire() as conn:
            return await conn.execute(*params)


    async def _query(self, *params):
        async with self.db.acquire() as conn:
            return await conn.fetch(*params)


    async def create_customer(self, email: str, name: str):
        try:
            id = uuid4().hex
            await self._execute(sql.NEW_CUSTOMER, id, email, name)
            return id

        except UniqueViolationError:
            raise ex.CustomerAlreadyExists


    async def list_customers(self):
        customers = await self._query(sql.LIST_CUSTOMERS)
        return [{'email': c['email'],
                 'name': c['name']} for c in customers]


    async def get_customer(self, id: str):
        result = await self._query(sql.GET_CUSTOMER_BY_ID, id)
        if not result:
            raise ex.CustomerNotFound

        return {'email': result[0]['email'], 'name': result[0]['name']}


    async def update_customer(self, id: str, email: str, name: str):
        if email:
            resp = await self._execute(sql.UPDATE_CUSTOMER_EMAIL, id, email)
            if resp == NO_RECORDS_UPDATED:
                raise ex.CustomerNotFound

        if name:
            resp = await self._execute(sql.UPDATE_CUSTOMER_NAME, id, name)
            if resp == NO_RECORDS_UPDATED:
                raise ex.CustomerNotFound


    async def delete_customer(self, id: str):
        resp = await self._execute(sql.DELETE_CUSTOMER_BY_ID, id)
        if resp == NO_RECORDS_DELETED:
            raise ex.CustomerNotFound


    async def add_favorite(self, customer_id: str, product_id: str):
        try:
            await self._execute(sql.ADD_FAVORITE, customer_id, product_id)

        except UniqueViolationError:
            raise ex.ProductAlreadyFavorite

        except ForeignKeyViolationError:
            raise ex.CustomerNotFound


    async def list_favorites(self, id: str):
        result = await self._query(sql.LIST_FAVORITES, id)

        if not result:
            raise ex.CustomerNotFound

        return {'product_ids': [r['product_id'] for r in result]}


    async def favorite_exists_or_raise(self, customer_id: str, product_id: str):
        result = await self._query(
            sql.FAVORITE_EXISTS, customer_id, product_id)

        if not result[0]['exists']:
            raise ex.ProductNotFavorite
