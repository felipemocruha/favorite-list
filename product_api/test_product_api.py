import pytest
from mock import MagicMock
import product_api


async def test_get_product_info_ok():
    expected = {
        'id': 'test',
        'title': 'Teste',
        'image': 'http://images.test.com/some-uuid',
        'price': 322.59
    }

    def json():
        return expected

    def get(*args, **kwargs):
        return MagicMock(status_code=200, json=json)

    client = MagicMock(get=get)
    api = product_api.ProductAPI(client, 'http://somewhere.com')
    info = api.get_product_info('1a2b3c')

    assert info == expected


def test_get_product_info_not_found():
    def get(*args, **kwargs):
        return MagicMock(status_code=404)

    client = MagicMock(get=get)
    api = product_api.ProductAPI(client, 'http://somewhere.com')

    with pytest.raises(product_api.ProductNotFound):
        api.get_product_info('1a2b3c')
