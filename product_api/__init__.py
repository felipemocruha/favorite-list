import json
from tenacity import retry, wait_fixed, retry_if_result


class ProductNotFound(Exception):
    pass


class ProductAPI:
    def __init__(self, client, base_url):
        self.url = base_url
        self.client = client

    @retry(wait=wait_fixed(5), retry=retry_if_result(lambda v: v is None))
    def get_product_info(self, id):
        try:
            resp = self.client.get(
                f'{self.url}/{id}', allow_redirects=True)
            if resp.status_code == 404:
                raise ProductNotFound

            return resp.json()

        except ProductNotFound:
            raise ProductNotFound
