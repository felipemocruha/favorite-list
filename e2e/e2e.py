import json
from uuid import uuid4
import requests


# testing application "most critical" path
def test_e2e():
    host = 'http://0.0.0.0:9595'
    auth_headers = {'X-API-Key': '5207b4a1-ab40-48dc-9405-393d37d26500'}

    print('creating customer...')
    customer = {'name': 'e2e tester', 'email': f'{uuid4().hex}@test.com'}
    resp = requests.post(
        f'{host}/customers', json=customer, headers=auth_headers)

    assert resp.status_code == 200
    customer_id = resp.json()['id']

    print('adding some products to favorites...')
    products = [
        'd52b8697-0c2d-f27e-975a-ae40da6c7bf8',
        '72da3a11-879c-f7eb-561e-f835033d9690',
        '09a782db-6e13-a400-1fc7-07e031353f7d'
    ]

    for p in products:
        payload = {'product_id': p}
        resp = requests.post(
            f'{host}/customers/{customer_id}/favorites',
            json=payload, headers=auth_headers
        )
        assert resp.status_code == 200

    print('getting favorite products list')
    resp = requests.get(
        f'{host}/customers/{customer_id}/favorites', headers=auth_headers)

    assert resp.status_code == 200
    favorites = resp.json()['product_ids']

    # now get a more detailed view
    favorites_view = []

    for p in favorites:
        resp = requests.get(
            f'{host}/customers/{customer_id}/favorites/{p}',
            headers=auth_headers
        )

        assert resp.status_code == 200
        favorites_view.append(resp.json())


    # now output the results
    print()
    print()
    print('RESULTS: ')
    print()
    print(json.dumps(favorites_view, indent=2))
    print()


try:
    test_e2e()

except Exception as err:
    print('failed to run e2e test: ', str(err))
