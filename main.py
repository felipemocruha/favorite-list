import os
import requests
from fastapi import FastAPI

from service import build_service
from repository import connect_to_db
from product_api import ProductAPI

default_config = {
    'db': {
        'host': '0.0.0.0',
        'port': '5432',
        'user': 'favorites',
        'password': 'favorites',
        'database': 'favorites'
    },
    'api_keys': {
        'admin': '5207b4a1-ab40-48dc-9405-393d37d26500',
        'reader': '6b41f0fb-a923-455e-8379-9915773fccf4'
    }
}


def get_config():
    path = os.getenv('CONFIG_PATH')
    if not path:
        return default_config

    try:
        return json.loads(open(path).read())
    except:
        return default_config


service = FastAPI(
    title="Favorite Products API",
    description="Service to store customers and their favorite products",
    version="1.0.0"
)

@service.on_event('startup')
async def main():
    config = get_config()
    db = await connect_to_db(config['db'])
    product_api = ProductAPI(requests,
                            "http://challenge-api.luizalabs.com/api/product")

    await build_service(service, db, product_api, config)
